#include <iostream>
#include <vector>
#include <algorithm>


template <typename Function, typename SecondArgType>
class partial_application_bind2nd_impl {
 public:

	partial_application_bind2nd_impl(Function function,
	                                 SecondArgType second_arg)
		: m_function(function)
		, m_second_arg(second_arg) {
	}

	// Pre-C++17 decltype
	// template <typename FirstArgType>
	// auto operator()(FirstArgType&& first_arg) const
	// -> decltype(m_function(
	//               std::forward<FirstArgType>(first_arg),
	//               this->m_second_arg)) {
	// 	return m_function(
	// 	         std::forward<FirstArgType>(first_arg),
	// 	         m_second_arg);
	// }

	// C++17 decltype(auto)
	template <typename FirstArgType>
	decltype(auto) operator()(FirstArgType&& first_arg) const {
		return m_function(
		         std::forward<FirstArgType>(first_arg),
		         m_second_arg);
	}



 private:
	Function m_function;
	SecondArgType m_second_arg;
};



class response_t {
 public:
	response_t	(std::string msg, bool error = false) : m_msg(msg), m_error(error)
	{}

	bool error() {
		return m_error;
	}

	friend std::ostream& operator << (std::ostream& os, response_t& response);

 private:
	std::string m_msg;
	bool m_error;
};

std::ostream& operator << (std::ostream& os, response_t& response) {
	os  << response.m_msg << " : " <<  response.m_error;
	return os;
}

class error_test_t {
 public:
	error_test_t	(bool error = true) : m_error(error)
	{}


	// filter(values, error/not_error))
	template<typename T>
	bool operator()(T&& value) const {
		return m_error == (bool)std::forward<T>(value).error();
	}

	// filter(responses, error == true/false, not_error == true/false)
	error_test_t operator==(bool test) const {
		return error_test_t(m_error == test ? true : false);
		//return error_test_t(test ? m_error : !m_error);
	}

	// filter(values, !error/!not_error)
	error_test_t operator!() const {
		return error_test_t(!m_error);
	}

 private:
	bool m_error;
};


template<typename	T>
T filter(T values, error_test_t pred) {
	T r{};
	for (auto v : values) {
		if (pred(v)) {
			r.push_back(v);
		}
	}
	return r;
}

int main () {
	error_test_t error(true);
	error_test_t not_error(false);

	auto responses = std::vector<response_t> {{"one", false}, {"two", true}, {"three", true}, {"four", false}};
	std::cout << "responses = ...\n";
	for (auto r : responses) {
		std::cout << r << std::endl;
	}
	std::cout << "==================" <<  std::endl;


	auto oks = filter(responses, error);
	std::cout << "error ...\n";
	for (auto ok : oks) {
		std::cout << ok << std::endl;
	}
	std::cout << std::endl;

	auto notoks = filter(responses, not_error);
	std::cout << "not_error ...\n";
	for (auto notok : notoks) {
		std::cout << notok << std::endl;
	}
	std::cout << std::endl;

	auto xs_true = filter(responses, error == true);
	std::cout << "error == true ...\n";
	for (auto x : xs_true) {
		std::cout << x << std::endl;
	}
	std::cout << std::endl;

	auto xs_false = filter(responses, error == false);
	std::cout << "error == false ...\n";
	for (auto x : xs_false) {
		std::cout << x << std::endl;
	}
	std::cout << std::endl;

	auto xs_ne_true = filter(responses, not_error == true);
	std::cout << "not_error == true ...\n";
	for (auto x : xs_ne_true) {
		std::cout << x << std::endl;
	}
	std::cout << std::endl;

	auto xs_ne_false = filter(responses, not_error == false);
	std::cout << "not_error == false ...\n";
	for (auto x : xs_ne_false) {
		std::cout << x << std::endl;
	}
	std::cout << std::endl;

	auto not_errors = filter(responses, !error);
	std::cout << "!error ...\n";
	for (auto x : not_errors) {
		std::cout << x << std::endl;
	}
	std::cout << std::endl;

	auto not_not_errors = filter(responses, !not_error);
	std::cout << "!not_error ...\n";
	for (auto x : not_not_errors) {
		std::cout << x << std::endl;
	}
	std::cout << std::endl;
}



template <typename T>
class space_vector {
 public:
	constexpr space_vector(std::initializer_list<T>)
	{}

};


template <typename T>
space_vector<T> external_acceleration2 = {T{}, T{ -9.8}, T{} };

// Requires constexpr ctor(?)
template <typename T>
constexpr	space_vector<T> external_acceleration = {T{}, T{ -9.8}, T{} };

auto acc = external_acceleration<float>;

template <typename T>
constexpr T viscosity = 0.4;

auto vis2 = 2 * viscosity<double>;